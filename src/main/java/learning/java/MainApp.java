package learning.java;

import com.google.common.io.Resources;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.URL;

/**
 *
 */
public class MainApp extends Application
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Override
	public void start(Stage primaryStage) throws Exception
	{
		URL mainForm = Resources.getResource("main.fxml");
		Scene scene = new Scene(FXMLLoader.<Parent>load(mainForm));
		primaryStage.setScene(scene);
		primaryStage.setTitle("Hello, i am new simple SSH client!");
		primaryStage.show();
	}
	
}
