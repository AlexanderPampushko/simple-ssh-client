package learning.java;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 */
public class MainController implements Initializable //интерфейс инициализации контроллера
{
	
	@FXML
	TextArea textArea;
	
	@FXML
	TextField inputTextField;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		Platform.runLater(new Runnable()
		{
			@Override
			public void run()
			{
				inputTextField.requestFocus();
			}
		});
	}
	
	public void addNewText(ActionEvent actionEvent)
	{
		TextField textField = (TextField) actionEvent.getSource();
		String text = textField.getText();
		if (text.trim().length() > 0)
		{
			textArea.appendText(text + System.lineSeparator());
		}
		textField.clear();
	}
}
